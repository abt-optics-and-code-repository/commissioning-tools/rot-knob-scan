import pyjapc
import numpy as np
#import accphylib.acc_library as al
import matplotlib.pyplot as plt
from cern_general_devices.bct import BCTDC
import pandas as pd

# from matplotlib.animation import FuncAnimation
import pickle
from scipy.stats import norm
import copy
import time
from scipy.optimize import curve_fit
#from tt20_tracking.kick_response_madx import ResponseMatrixTT20
import datetime
import os
from glob import glob

# intensity_var_name = 'SEM_CONCENTRATOR_TT20/BeamIntensity#intensity'
# extracted_intensity_name = 'SPS.BCTDC.51454/Acquisition#slowExtInt'

default_scanner_settings = dict(
    timing_user = "SPS.USER.SFTPRO1",
    subscriptions = dict(
        grids = "SEM_CONCENTRATOR_TT20/Grids",
        total_intensity = "SPS.BCTDC.51454/Acquisition#totalIntensity",
    ),
    grid_names = ['BSGH.21632', 'BSGH.21856'],
    to_vary = dict(
        rot_device = 'NORTH_EXTRBEAM/MD_2018_LSE_1deg',
        bump_device = 'NORTH_EXTRBEAM/LSS2_EXTR_BUMP_NOQSPLIT'
    ),
    metadata_names = [],
    dump_on_disc = ['grids', 'total_intensity'],
    out_dir = '/afs/cern.ch/user/g/gorna/afs_work/octupole-folding-mds/'
)
class Scanner():
    def __init__(self, noSet=True, scanner_settings=default_scanner_settings):
        for key, value in scanner_settings.items():
            setattr(self, key, value)
        self.rot_device = self.to_vary['rot_device']
        self.bump_device = self.to_vary['bump_device']
        self.japc = pyjapc.PyJapc(self.timing_user, noSet=noSet)
        self.bct = BCTDC(self.japc, timingSelectorOverride=self.timing_user)
        self.data_temp = self.japc.getParam(self.rot_device, timingSelectorOverride=self.timing_user)
        self.data_template = np.array(
            [
                [0.0, 2230.0, 4460.0, 9240.0, 10020.0, 10800.0],
                [
                    np.NaN,
                    np.NaN,
                    self.data_temp[1][2],
                    self.data_temp[1][2],
                    np.NaN,
                    np.NaN,
                ],
            ]
        )
        self.cycle_valid = False

    def transform_data_to_set(self, value_to_set, relative=True):
            data = copy.copy(self.data_template)
            #offset = self.data_start if relative else 0.0
            data[1, 2] = value_to_set# + offset
            data[1, 3] = value_to_set# + offset
            return data

    def get_device_value(self, device_name):
        data_raw = self.japc.getParam(
            device_name, timingSelectorOverride=self.timing_user
        )
        return data_raw[1][2]
    
    def set_device(self, device_name, value_to_set, relative=True):
        data_to_set = self.transform_data_to_set(
            value_to_set, relative=relative
        )
        print(
            f"Device {device_name} being set to {value_to_set} for user: {self.timing_user}"
        )
        self.japc.setParam(
            device_name,
            data_to_set,
            timingSelectorOverride=self.timing_user,
        )
    
    def set_rot(self, value_to_set, relative=True):
        data_to_set = self.transform_data_to_set(
            value_to_set, relative=relative
        )
        print(
            f"Device {self.rot_device} being set to {value_to_set} for user: {self.timing_user}"
        )
        self.japc.setParam(
            self.rot_device,
            data_to_set,
            timingSelectorOverride=self.timing_user,
        )

    def get_rot(self):
        data_raw = self.japc.getParam(
            self.rot_device, timingSelectorOverride=self.timing_user
        )
        return data_raw[1][2]

    def set_bump(self, value_to_set, relative=True):
        data_to_set = self.transform_data_to_set(
            value_to_set, relative=relative
        )
        print(
            f"Device {self.bump_device} being set to {value_to_set} for user: {self.timing_user}"
        )
        self.japc.setParam(
            self.bump_device,
            data_to_set,
            timingSelectorOverride=self.timing_user,
        )

    def get_bump(self):
        data_raw = self.japc.getParam(
            self.bump_device, timingSelectorOverride=self.timing_user
        )
        return data_raw[1][2]  
    
    def make_callback(self, skip_validation=False):
        def callback(param, value, header):
            global cycle_valid
            cycle_valid = False
            cycle_stamps = [head["cycleStamp"] for head in header]
            
            # Get extracted beam intensity
            try:
                intensity, info_int = self.bct.get_extracted_intensity(getHeader=True)
            except TypeError:
                intensity = 0.0

            # Check if cycle is valid
            isFirstUpdate = header[0]["isFirstUpdate"]
            invalidCycleStamps = [
                cycle_stamp != header[0]["cycleStamp"]
                for cycle_stamp in cycle_stamps
            ]
            if sum(invalidCycleStamps) > 0:
                print(f"Warning! Invalid cycle stamps: {param}, {cycle_stamps}")

            # rightCycleStamps = all(
            #     [
            #         cycle_stamp == header[0]["cycleStamp"]
            #         for cycle_stamp in cycle_stampss
            #     ]
            # )
            highEnoughInt = intensity > 2e11
            
            validate_cycle = (
                not isFirstUpdate
                # and rightCycleStamps
                and highEnoughInt
            )

            if skip_validation:
                validate_cycle = True

            print('intensity', intensity)
            
            if validate_cycle:
                print('good cycle!')
                metadata = {
                    'intensity': intensity,
                }
                data = dict(dict(zip(param, value)))
                self.acquire_data(data, metadata)
                self.cycle_valid = True    
                return True
            else:
                reason = ''
                if isFirstUpdate:
                    reason += 'first update; '
                # if not rightCycleStamps:
                #     reason += 'wrong cycle stamps; '
                if not highEnoughInt:
                    reason += 'low intensity;' 
                print('Bad cycle:', reason )
                return False
        return callback
    
    def scan(self, values, show_grids=False, rbacLogin=False, skip_validation=False):
        ct = datetime.datetime.now()
        yyyymmdd = str(ct)[:10]
        ts0 = time.time()

        # Initiate data acquisition
        self.cycle_valid = False
        if rbacLogin:
            self.japc.rbacLogin()
        self.japc.subscribeParam(list(self.subscriptions.values()), self.make_callback(skip_validation), getHeader=True)
        self.japc.startSubscriptions()

        # Separatrix rotation knob scan 
        def wait_for_good_cycle():
            while self.cycle_valid is False:
                print("waiting for a good cycle")
                time.sleep(3)
            self.cycle_valid = False

        if values is not None:
            # Save initial values
            self.initial_devices_values = {
                name: self.get_device_value(self.to_vary[name]) for name in self.to_vary.keys()
            }
            
            # Prepare iterable grid
            if len(values.keys()) > 1:
                parametric_grid = np.meshgrid(*values.values())
                iterable = np.nditer(parametric_grid)
            elif len(values.keys()) <= 1:
                iterable = np.array([list(values.values())[0]]).T
            
            # Vary values
            for vals in iterable:
                for i, value in enumerate(vals):
                    print(value)
                    value = float(value)
                    name = list(values.keys())[i]
                    device_name = self.to_vary[name]
                    self.set_device(device_name, value)
                wait_for_good_cycle()
            
            # Set initial values
            for name, value in self.initial_devices_values.items():
                device_name = self.to_vary[name]
                self.set_device(device_name, value)
        else:
            wait_for_good_cycle()
        
        # Stop subscriptions
        self.japc.stopSubscriptions()
        
        # Plot acquired data
        self.plot_data(ts0, yyyymmdd, show_grids=show_grids)

    def acquire_data(self, data, metadata={}):
        # Ascquires data from callback

        # Prepare directory
        ct = datetime.datetime.now()
        yyyymmdd = str(ct)[:10]  
        ts = time.time()
        
        out = self.out_dir + yyyymmdd + '/'
        if yyyymmdd not in os.listdir(self.out_dir):
            os.mkdir(out)

        # Collect metadata
        metadata['time'] = ct
        for name in self.metadata_names:
            metadata[name] = data[self.subscriptions[name]]
        for name in self.to_vary.keys():
            metadata[name] = self.get_device_value(self.to_vary[name])

        # Save metadata
        df = pd.DataFrame(metadata, index=[ts])
        df.to_pickle(out + f"{ts}-metadata.pkl")
        print('Metadata saved to', out + f"{ts}-metadata.pkl")
        print(df.to_string())

        # Dump measurements to disc
        for name in self.dump_on_disc:
            par = self.subscriptions[name]
            prefix = f"{ts}-{name}"
            with open(out + prefix + ".pkl", "wb") as output_file: 
                pickle.dump(data[par], output_file)
            print(f"{name} saved to", out + prefix + ".pkl")
    
    def plot_data(self, starting_from, yyyymmdd, show_grids=False):
        # Plots grids and shows metadata after scan
        def plot_grids(grid_data, grid_names, prefix=None, show_grids=True):
            for i, grid_name in enumerate(grid_names):
                x, y, y_fit = grid_data[grid_name]
                x, y, y_fit = x[::-1], y[::-1], y_fit[::-1]
                x = -x 
                x -= x[0]
                dx = x[1] - x[0]
                edges = np.arange(x[0] - dx/2., x[-1] + dx/2. + dx*0.01, dx)
                fig = plt.figure(i)
                plt.title(grid_name)
                plt.hist(x, bins=edges, weights=y)
                plt.plot(x, y, 'o', label='measurements')
                #plt.plot(x, y_fit, label='gauss fit')
                plt.legend()
                plt.tight_layout()
                plt.savefig(str(prefix) + f'-{grid_name}.png')
                print('Plot saved to', str(prefix) + f'-{grid_name}.png')
                if not show_grids:
                    plt.close()

        for grid_filename in glob(self.out_dir + yyyymmdd + '/*grids*.pkl'):
            ts = grid_filename.split('/')[-1].split('-')[0]
            
            if float(ts) < float(starting_from):
                continue
            with open(grid_filename, 'rb') as f:
                grid_data = pickle.load(f)
            prefix = grid_filename.replace('.pkl', '')
            plot_grids(grid_data, self.grid_names, prefix=prefix, show_grids=show_grids)
            if show_grids:
                plt.show()
            else:
                plt.close() 

############# Scan ##############

scanner_settings = dict(
    timing_user = "SPS.USER.SFTPRO1",
    subscriptions = dict(
        grids = "SEM_CONCENTRATOR_TT20/Grids",
        total_intensity = "SPS.BCTDC.51454/Acquisition#totalIntensity",
        bsis = "SEM_CONCENTRATOR_TT20/SingleFoilsRaw",
        sx_monitoring = "SPS.SX.monitoring/Acquisition",
    ),
    grid_names = ['BSGH.21632', 'BSGH.21856'],
    bsi_names = ['BSIA.210272', 'BSIA.210278', 'BSIA.210279', 'BSIA.220412'],
    to_vary = dict(
        rot_device = 'NORTH_EXTRBEAM/MD_2018_LSE_1deg',
        bump_device = 'NORTH_EXTRBEAM/LSS2_EXTR_BUMP_NOQSPLIT',
        sext_device = 'NORTH_EXTRBEAM/EXTR_SEXT_NOQSPLIT'
    ),
    metadata_names = [],
    dump_on_disc = ['grids', 'total_intensity', 'bsis', 'sx_monitoring'],
    #out_dir = '/afs/cern.ch/user/g/gorna/afs_work/octupole-folding-mds/'
    out_dir = './',
)

scanner = Scanner(scanner_settings=scanner_settings, noSet=False)

values = dict(
    rot_device = np.arange(-80, 41, 10),
    #bump_device = np.linspace(1, 1.1, 2),
    sext_device = np.arange(0.8, 1.01, 0.05),
)

#values = None

scanner.scan(values=values, show_grids=False, rbacLogin=True, skip_validation=False)
